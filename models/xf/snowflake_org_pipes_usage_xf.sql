WITH base AS (

	SELECT *
	FROM {{ ref('snowflake_org_pipe_usage_history') }}

), contract_rates AS (

    SELECT *
    FROM {{ ref('snowflake_amortized_rates') }}

), usage AS (

    SELECT
      base.account_name,
      base.pipe_id,
      base.pipe_name,
      base.usage_date,
      DATE_TRUNC('month', base.usage_date)::DATE          AS usage_month,
      DATE_TRUNC('day', base.usage_date)::DATE            AS usage_day,
      contract_rates.rate                               AS credit_rate,
      ROUND(base.credits_used * contract_rates.rate, 2) AS dollars_spent
    FROM base
    LEFT JOIN contract_rates
      ON usage_date = contract_rates.date_day

)

SELECT *
FROM usage
