{% docs snowflake_warehouse_metering %}

This is the base model for the Snowflake Warehouse Metering History table https://docs.snowflake.net/manuals/sql-reference/account-usage/warehouse_metering_history.html.

{% enddocs %}


{% docs snowflake_query_history %}

This is the base model for the Snowflake Query History table https://docs.snowflake.com/en/sql-reference/functions/query_history.html.

{% enddocs %}

{% docs snowflake_pipes %}

This is the base model for information on Snowflake Pipes https://docs.snowflake.com/en/sql-reference/account-usage/pipes.html.

{% enddocs %}

{% docs snowflake_pipe_usage_history %}

This is the base model for the Snowflake Pipe Usage History table https://docs.snowflake.net/manuals/sql-reference/account-usage/pipe_usage_history.html.

{% enddocs %}
