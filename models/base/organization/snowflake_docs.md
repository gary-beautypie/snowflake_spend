{% docs snowflake_org_warehouse_metering %}

This is the base model for the Snowflake Warehouse Metering History table https://docs.snowflake.net/manuals/sql-reference/account-usage/warehouse_metering_history.html.

{% enddocs %}

{% docs snowflake_org_pipe_usage_history %}

This is the base model for the Snowflake Pipe Usage History table https://docs.snowflake.net/manuals/sql-reference/account-usage/pipe_usage_history.html.

{% enddocs %}
