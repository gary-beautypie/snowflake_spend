WITH base AS (

	SELECT *
	FROM {{ source('snowflake_org','pipe_usage_history') }}

)

SELECT
  account_name,
  pipe_id,
  pipe_name,
  usage_date,
  credits_used
FROM base
