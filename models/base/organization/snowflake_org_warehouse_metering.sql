WITH base AS (

	SELECT *
	FROM {{ source('snowflake_org','warehouse_metering_history') }}

)

SELECT
  account_name,
  warehouse_id,
  warehouse_name,
  start_time,
  end_time,
  credits_used
FROM base
